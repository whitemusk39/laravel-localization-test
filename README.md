Laravel Localization Test using Translation Strings in JSON file.

# Files of Interest
* /app/Http/Middleware/LoadLocale.php: Used to set and load the locale saved in the session
* /app/Http/Kernel.php: Loads the middleware in the `$middleware` property.
* /resources/lang/my.json: Custom locale file for Bahasa Melayu
* /resources/views/welcome.blade.php: Line `#84` shows an example to display a localised string.
* /routes/web.php: Second route (line `#20`) is the route used to set the locale.